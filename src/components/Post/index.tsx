import Link from 'next/link';
import { useRouter } from 'next/router';
import { FaEdit } from 'react-icons/fa';
import { MdDeleteForever } from 'react-icons/md';

import styles from './styles.module.scss';

type Post = {
  id: number;
  title: string;
  text: string;
  date: string;
}

interface PostProps {
  post: Post;
  handleDelete: () => Promise<void>;
}

export function Post({ post, handleDelete }: PostProps) {
  
  function handleEdit(post: Post) {
    localStorage.setItem('post', JSON.stringify(post));
  }
  
  return (
    <div className={styles.container}>
      <div className={styles.headerContainer}>
        <h3>{new Date(post.date).toLocaleDateString('pt-BR', {
          day: '2-digit',
          month: 'long',
          year: 'numeric'
        })}</h3>

        <div className={styles.options}>
          <Link href={`/posts/${post.id}`}>
            <a>
              <FaEdit
                className="edit"
                onClick={() => handleEdit(post)}
                size={20}
              />
            </a>
          </Link>

          <MdDeleteForever
            className="delete"
            size={24}
            onClick={handleDelete}
          />
        </div>
      </div>

      <h2 className={styles.title}>{post.title}</h2>
      
      <article>
        {post.text}
      </article>
    </div>
  );
}