import { useRef, useState } from 'react';

import { useRouter } from 'next/router';
import { GoNote } from 'react-icons/go';

import { api } from '../../services/api';

import styles from './styles.module.scss';

type User = {
  id: number;
  email: string;
}

export function SignIn() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const inputEmail = useRef(null);
  const inputPassword = useRef(null);

  const router = useRouter();

  async function handleSignIn(event): Promise<void> {
    event.preventDefault();

    if (!email) {
      inputEmail.current.focus();
      return;
    }

    if (!password) {
      inputPassword.current.focus();
      return;
    }

    try {
      await api.post('/auth/login', { email, password })
        .then(response => {
          if (response.status === 200) {
            localStorage.setItem('token', response.data.access_token);
          }
        })
        .catch(err => alert(err));
      
      const users: User[] = await api.get('/users',
        {
          headers: { Authorization: `Bearer ${localStorage.getItem('token')}`}
        }).then(response => Object.values(response.data));
      
      if (users) {
        const user = users.find(user => user.email === email);

        if (!user) {
          alert('Usuário não localizado');
          localStorage.setItem('token', '');
          localStorage.setItem('id_use', '');
          return;
        }

        localStorage.setItem('id_use', user.id.toString());
        router.push('/posts');
      }
    } catch (err) {
      localStorage.setItem('token', '');
      localStorage.setItem('id_use', '');
      alert(err.message);
    }
  }

  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <div className={styles.title}>
          <h1>Posting Wall</h1>
          <GoNote size={35} />
        </div>

        <form className={styles.form}>
          <input
            type="email"
            value={email}
            ref={inputEmail}
            placeholder="E-mail: posting@wall.com.br"
            onChange={(e) => setEmail(e.target.value)}
          />
          
          <input
            type="password"
            value={password}
            ref={inputPassword}
            placeholder="Senha: ************"
            onChange={(e) => setPassword(e.target.value)}
          />
          
          <button
            type="submit"
            onClick={handleSignIn}
          >
            Acessar
          </button>
        </form>
      </div>
    </div>
  );
}

