import { useRouter } from 'next/router';
import { GoNote } from 'react-icons/go';
import { FaSignOutAlt } from 'react-icons/fa';

import styles from './styles.module.scss';

export function Header() {
  const router = useRouter();

  function handleSignOut(event) {
    event.preventDefault();
    localStorage.setItem('token', '');
    localStorage.setItem('id_use', '');
    router.push('/');
  }

  return (
    <header>
      <div className={styles.headerContent}>
        <div className={styles.logo}>
          <GoNote size={35} />
          <h1>Posting Wall</h1>
        </div>

        <button onClick={(e) => handleSignOut(e)}>
          <FaSignOutAlt size={20} />
        </button>
      </div>
    </header>
  );
}