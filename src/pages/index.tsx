import React from "react";

import Head from "next/head";

import { SignIn } from "../components/SignIn";

export default function Home() {
  return (
    <>
      <Head>
        <title>SignIn | Posting Wall</title>
      </Head>
      <SignIn />
    </>
  );
}
