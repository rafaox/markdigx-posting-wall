import React, { useEffect, useRef, useState } from 'react';

import NextLink from 'next/link';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { IoArrowBack } from 'react-icons/io5';

import { Header } from '../../components/Header';
import { api } from '../../services/api';

import styles from '../../styles/posts/post.module.scss';

type Category = {
  id: number;
  name: string;
}

type Post = {
  id?: number;
  title: string;
  text: string;
  id_category: number;
  id_user: number;
  date: string;
}

interface CreatePostProps {
  post: Post;
  isEdit: boolean;
}

export default function EditPost({ post }: CreatePostProps) {
  let d = new Date(),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

  if (month.length < 2) 
      month = '0' + month;
  if (day.length < 2) 
      day = '0' + day;

  const minDate = `${year}-${month}-${day}`;

  const [categories, setCategories] = useState<Category[]>([]);

  post = JSON.parse(localStorage.getItem('post'));
  
  const [title, setTitle] = useState(post.title);
  const [category, setCategory] = useState(post.id_category);
  const [text, setText] = useState(post.text);
  const [schedulingDate, setSchedulingDate] = useState(post.date);

  const inputTitle = useRef(null);
  const inputText = useRef(null);

  const router = useRouter();

  useEffect(() => {
    api.get('/categories',
      {
        headers: { Authorization: `Bearer ${localStorage.getItem('token')}`}
      })
      .then((response => {
        if (response.status === 200)
          setCategories(response.data);
      }))
      .catch((error) => alert(error));
  }, []);

  async function handleClick(event) {
    event.preventDefault();
    
    if (!title) {
      inputTitle.current.focus();
      return;
    }

    if (!text) {
      inputText.current.focus();
      return;
    }

    try {
      await api.post('/posts',
        {
          id: post.id,
          title: title,
          text: text,
          id_category: category,
          id_user: Number(localStorage.getItem('id_use')),
          date: schedulingDate,
        },
        {
          headers: { Authorization: `Bearer ${localStorage.getItem('token')}`}
        })
        .then(response => {
          if (response.status === 201)
            router.push('/posts')
        })
        .catch(err => alert(err));
    } catch (err) {
       alert(err.message);
    }
  }

  return (
    <>
      <Head>
        <title>{post.title} | Ignews</title>
      </Head>
      
      <Header />
      
      <div className={styles.wrapper}>
        <NextLink href="/posts">
          <button className={styles.back}>
            Voltar
            <IoArrowBack size={20} />
          </button>
        </NextLink>

        <form className={styles.container}>
          
          <label htmlFor="title">Título</label>
          <input
            type="text"
            id="title"
            value={title}
            ref={inputTitle}
            onChange={(e) => setTitle(e.target.value)}
          />

          <div className={styles.orientation}>
            <div className={styles.verticalOrientation}>
              <label htmlFor="category">Categoria</label>
              <select
                value={category}
                onChange={(e) => setCategory(Number(e.target.value))}
                className={styles.categories}
              >
                { categories && categories.map(category => (
                  <option
                    key={category.id}
                    value={category.id}
                  >
                    {category.name}
                  </option>
                ))}
              </select>
            </div>

            <div className={styles.verticalOrientation}>
              <label htmlFor="schedulingDate">Data de agendamento</label>
              <input
                type="date"
                id="schedulingDate"
                value={schedulingDate}
                min={minDate}
                onChange={(e) => setSchedulingDate(e.target.value)}
              />
            </div>
          </div>

          <label htmlFor="text">Postagem</label>
          <textarea
            id="text"
            value={text}
            ref={inputText}
            onChange={(e) => setText(e.target.value)}
          />

          <button
            type="submit"
            onClick={(e) => handleClick(e)}
          >
            Cadastrar
          </button>
        </form>
      </div>
    </>
  );
}