import React, { useEffect, useState } from "react";

import NextLink from 'next/link';

import { api } from "../../services/api";
import { Header } from "../../components/Header";
import { Post } from "../../components/Post";

import styles from '../../styles/posts/styles.module.scss';

type Post = {
  id: number;
  title: string;
  text: string;
  id_category: number;
  id_user: number;
  date: string;
}

type Category = {
  id: number;
  name: string;
}

export default function Posts() {
  const [posts, setPosts] = useState<Post[]>();
  const [categories, setCategories] = useState<Category[]>([]);
  const [idCategory, setIdCategory] = useState(0);

  useEffect(() => {
    api.get(
      '/posts',
      {
        headers: { Authorization: `Bearer ${localStorage.getItem('token')}`}
      })
      .then(response => {
        if (response.status === 200)
          setPosts(response.data);
      })
      .catch(error => alert(error));
  }, []);

  useEffect(() => {
    api.get('/categories',
      {
        headers: { Authorization: `Bearer ${localStorage.getItem('token')}`}
      })
      .then((response => {
        if (response.status === 200){
          const fistOption: Category = { id: 0, name: 'Todos' };
          setCategories([fistOption, ...response.data]);
        }
      }))
      .catch((error) => alert(error));
  }, []);

  async function handleDelete(id: number): Promise<void> {
    try {
      const response = await api.delete(`/posts/${id}`,
        {
          headers: { Authorization: `Bearer ${localStorage.getItem('token')}`}
        });
      
      if (response.status === 200) {
        const newPosts: Post[] = posts.filter(post => post.id !== id);
        setPosts(newPosts);
      }
    } catch (err) {
      alert(err);
    }
  }

  return (
    <>
      <Header />

      <div className={styles.action}>
        <NextLink
          href="/posts/create"
          passHref
        >
          <button className={styles.new}>Nova postagem</button>
        </NextLink>

        <select
          onChange={(e) => setIdCategory(Number(e.target.value))}
          className={styles.categories}
        >
          { categories && categories.map(category => (
            <option
              key={category.id}
              value={category.id}
            >
              {category.name}
            </option>
          ))}
        </select>
      </div>

      <main className={styles.wall}>
        <div className={styles.container}>
          { posts && posts.filter(post => new Date(post.date) <= new Date() &&
                                          (idCategory === 0 && post.id_category !== 0) ||
                                          (idCategory !== 0 && post.id_category === idCategory))
            .map(post => (
            <Post
              key={post.id}
              post={post}
              handleDelete={() => handleDelete(post.id)}
            />
          ))}
        </div>
      </main>
    </>
  );
}